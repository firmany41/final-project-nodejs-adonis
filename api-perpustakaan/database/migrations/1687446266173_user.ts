import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'user'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('nama', 45).notNullable()
      table.string('email', 45).notNullable().unique()
      table.string('password', 125).notNullable()
      table.enu('role', ['admin', 'user']).notNullable()
      table.boolean('is_verified').defaultTo(false)

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamps(true, true);
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
