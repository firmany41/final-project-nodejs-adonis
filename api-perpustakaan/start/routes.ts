/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.group(() => {

    Route.group(() => {

    //Kategori
    Route.resource('kategori', 'KategoriController').apiOnly()

    //Buku
    Route.resource('buku', 'BukuController').apiOnly()

    //user
    Route.get('/user', 'UserController.index')
    Route.get('/user/:id', 'UserController.show')
    Route.put('/user/:id', 'UserController.update')
    Route.delete('/user/:id', 'UserController.destroy')    

    }).middleware('auth').middleware('role').middleware('verify')

    //Peminjaman
    Route.post('/buku/:id/peminjaman', 'PeminjamanController.store').middleware('auth').middleware('verify')
    Route.get('/peminjaman', 'PeminjamanController.index').middleware('auth').middleware('verify')
    Route.get('/peminjaman/:id', 'PeminjamanController.show').middleware('auth').middleware('verify')
    Route.put('/peminjaman/:id', 'PeminjamanController.update').middleware('auth').middleware('verify')
    Route.delete('/peminjaman/:id', 'PeminjamanController.destroy').middleware('auth').middleware('verify')

    //Auth
    Route.group(() => {
        Route.post('/register', 'AuthController.register')
        Route.post('/login', 'AuthController.login')
        Route.post('/otp-confirmation', 'AuthController.otpConfirmation')
        Route.get('/me', 'AuthController.me').middleware('auth')
        Route.post('/profile', 'AuthController.updateProfile').middleware('auth')
    }).prefix('/auth')


}).prefix('/api/v1')

//Auth