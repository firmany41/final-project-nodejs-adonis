import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import BukuValidator from 'App/Validators/BukuValidator';
import Buku from 'App/Models/Buku';

export default class BukuController {
  public async store({ request, response }: HttpContextContract) {
    const payloadValidationBuku = await request.validate(BukuValidator);

    await Buku.create(payloadValidationBuku)

    return response.created({
      message: 'Buku Berhasil Ditambahkan'
    })
  }

  public async index({ response }: HttpContextContract) {
    const buku = await Buku.query().preload('kategori').orderBy('id', 'asc')
    return response.ok({
      message: 'Buku Berhasil Ditampilkan',
      data: buku
    })

  }

  public async show({ response, params }: HttpContextContract) {
    const idBuku = params.id;
    const indexBuku = await Buku.findBy('id', idBuku)
    const buku = await Buku.query().preload('kategori').preload('peminjaman').where('id', idBuku)

    if (!indexBuku) {
      return response.notFound({
        message: 'Data Tidak Ditemukan'
      })
    } else {
      return response.ok({
        message: 'Detail Buku Berhasil Ditampilkan',
        data: buku
      })
    }
  }

  public async update({ request, response, params }: HttpContextContract) {
    const idBuku = params.id;
    let buku = await Buku.findBy('id', idBuku)
    if (!buku) {
      return response.notFound({
        message: 'Buku Tidak Ditemukan'
      })
    } else {
      const payloadValidationBuku = await request.validate(BukuValidator);
      await Buku
        .query()
        .where('id', idBuku)
        .update(payloadValidationBuku)

      return response.ok({
        message: `Buku id = ${idBuku} Berhasil Diupdate`
      })
    }
  }

  public async destroy({ response, params }: HttpContextContract) {
    const idBuku = params.id;
    const buku = await Buku.findOrFail(idBuku)
    await buku.delete()

    return response.ok({
      message: `Buku id = ${idBuku} Berhasil Dihapus`
    })
  }
}
