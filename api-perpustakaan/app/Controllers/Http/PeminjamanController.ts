import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Peminjaman from 'App/Models/Peminjaman';
import { schema } from '@ioc:Adonis/Core/Validator'

export default class PeminjamanController {
    public async store({ params, request, response, auth }: HttpContextContract) {
        const user = auth.user?.id
        const peminjamanValidation = schema.create({
            tanggal_pinjam: schema.date({
                format: 'sql',
            }),
            tanggal_kembali: schema.date({
                format: 'sql',
            })
        })

        await request.validate({ schema: peminjamanValidation })

        await Peminjaman.create({
            user_id: user,
            buku_id: params.id,
            tanggal_pinjam: request.input('tanggal_pinjam'),
            tanggal_kembali: request.input('tanggal_kembali')
        })

        return response.created({
            message: 'Peminjaman Berhasil Ditambahkan'
        })
    }

    public async index({ response }: HttpContextContract) {
        const peminjaman = await Peminjaman.query().preload('user').preload('buku').orderBy('id', 'asc')

        return response.ok({
            message: 'Peminjaman Berhasil Ditampilkan',
            data: peminjaman
        })

    }

    public async show({ response, params }: HttpContextContract) {
        const idPeminjaman = params.id;
        const indexPeminjaman = await Peminjaman.findBy('id', idPeminjaman)
        const peminjaman = await Peminjaman.query().preload('user').preload('buku').where('id', idPeminjaman)

        if (!indexPeminjaman) {
            return response.notFound({
                message: 'Data Tidak Ditemukan'
            })
        } else {

            return response.ok({
                message: 'Detail Peminjaman Berhasil Ditampilkan',
                data: peminjaman
            })
        }
    }

    public async update({ request, response, params }: HttpContextContract) {
        const idPeminjaman = params.id;
        const peminjamanValidation = schema.create({
            tanggal_pinjam: schema.date({
                format: 'sql',
            }),
            tanggal_kembali: schema.date({
                format: 'sql',
            })
        })

        await request.validate({ schema: peminjamanValidation })

        let tanggal_pinjam = request.input('tanggal_pinjam')
        let tanggal_kembali = request.input('tanggal_kembali')
        await Peminjaman
            .query()
            .where('id', idPeminjaman)
            .update({
                'tanggal_pinjam': tanggal_pinjam,
                'tanggal_kembali': tanggal_kembali
            })
        return response.ok({
            message: `Peminjaman id = ${idPeminjaman} Berhasil Diupdate`
        })
    }

    public async destroy({ response, params }: HttpContextContract) {
        const idPeminjaman = params.id;
        const peminjaman = await Peminjaman.findOrFail(idPeminjaman)
        await peminjaman.delete()

        return response.ok({
            message: `Peminjaman id = ${idPeminjaman} Berhasil Dihapus`
        })
    }
}
