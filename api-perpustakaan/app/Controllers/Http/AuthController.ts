import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import User from 'App/Models/User';
import Mail from '@ioc:Adonis/Addons/Mail'
import Database from '@ioc:Adonis/Lucid/Database';

export default class AuthController {
    public async register({ request, response }: HttpContextContract) {
        try {
            const registerValidation = schema.create({
                email: schema.string({}, [
                    rules.email(),
                    rules.unique({
                        table: 'user',
                        column: 'email'
                    })
                ]),
                nama: schema.string(),
                password: schema.string({}, [
                    rules.minLength(8)
                ]),
                role: schema.enum(
                    ['admin', 'user'] as const
                )
            })

            const payloadValidation = await request.validate({ schema: registerValidation })
            const newUser = await User.create(payloadValidation)

            const otp_code = Math.floor(100000 + Math.random() * 9000000)

            await Database.table('otp_codes').insert({ otp_code: otp_code, user_id: newUser.id })

            await Mail.send((message) => {
                message
                    .from('admin@example.com')
                    .to(payloadValidation['email'])
                    .subject('Welcome Onboard!')
                    .htmlView('emails/otp_verification', { otp_code })
            })

            return response.created({
                message: 'Register Berhasil, Silahkan Verifikasi Kode OTP'
            })
        } catch (error) {
            if (error.guard) {
                return response.badRequest({
                    message: 'Register Gagal',
                    error: error
                })
            } else {
                return response.badRequest({
                    message: 'Register Gagal',
                    error: error
                })
            }
        }
    }

    public async login({ request, response, auth }: HttpContextContract) {
        try {
            const loginValidation = schema.create({
                email: schema.string(),
                password: schema.string(),
            })
            await request.validate({ schema: loginValidation })

            const email = request.input('email')
            const password = request.input('password')

            const token = await auth.use('api').attempt(email, password, {
                expiresIn: '7 days',
            })

            return response.created({
                message: 'Login Berhasil',
                token
            })

        } catch (error) {
            if (error.guard) {
                return response.badRequest({
                    message: 'Login Gagal',
                    error: error
                })
            } else {
                return response.badRequest({
                    message: 'Login Gagal',
                    error: error
                })
            }
        }
    }

    public async me({ auth, response }: HttpContextContract) {
        const user = auth.user;
        return response.ok({
            message: user
        })
    }

    public async updateProfile({ auth, response, request }: HttpContextContract) {
        const user = auth.user;
        const profileValidation = schema.create({
            bio: schema.string(),
            alamat: schema.string(),
        })
        await request.validate({ schema: profileValidation })

        const bio = request.input('bio')
        const alamat = request.input('alamat')
        const persistancePayload = {
            bio,
            alamat
        }

        await user?.related('profile').updateOrCreate({}, persistancePayload)


        return response.ok({
            message: "Sukses Tambah/Edit Profile"
        })
    }
    public async otpConfirmation({ response, request }: HttpContextContract) {
        const otpValidation = schema.create({
            email: schema.string(),
            otp_code: schema.string(),
        })
        await request.validate({ schema: otpValidation })

        let email = request.input('email')
        let otp_code = request.input('otp_code')

        let user = await User.findBy('email', email)
        let otpCheck = await Database.query().from('otp_codes').where('otp_code', otp_code).first()
        if (user?.id == otpCheck.user_id) {
            await User
                .query()
                .where('email', email)
                .update('is_verified', true)
            return response.ok({
                message: 'Konfirmasi OTP Berhasil'
            })
        } else {
            return response.badRequest({
                message: 'Konfirmasi OTP Gagal',
                user: user,
                otp: otpCheck
            })
        }
    }
}
