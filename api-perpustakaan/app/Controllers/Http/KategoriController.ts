import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';
import KategoriValidator from 'App/Validators/KategoriValidator';
import Kategori from 'App/Models/Kategori';

export default class KategoriController {
  public async store({ request, response }: HttpContextContract) {
    const payloadValidationKategori = await request.validate(KategoriValidator);

    await Kategori.create(payloadValidationKategori)

    return response.created({
      message: 'Kategori Berhasil Ditambahkan'
    })
  }

  public async index({ response }: HttpContextContract) {
    const kategori = await Kategori.query().preload('buku').orderBy('id', 'asc')

    return response.ok({
      message: 'Kategori Berhasil Ditampilkan',
      data: kategori
    })

  }

  public async show({ response, params }: HttpContextContract) {
    const idKategori = params.id;
    const indexKategori = await Kategori.findBy('id', idKategori)
    const kategori = await Kategori.query().preload('buku').where('id', idKategori)

    if (!indexKategori) {
      return response.notFound({
        message: 'Data Tidak Ditemukan'
      })
    } else {
      return response.ok({
        message: 'Detail Kategori Berhasil Ditampilkan',
        data: kategori
      })
    }
  }

  public async update({ request, response, params }: HttpContextContract) {
    const idKategori = params.id;
    let kategori = await Kategori.findBy('id', idKategori)

    if (!kategori) {
      return response.notFound({
        message: 'Kategori Tidak Ditemukan'
      })
    } else {
      const payloadValidationKategori = await request.validate(KategoriValidator);
      await Kategori
        .query()
        .where('id', idKategori)
        .update(payloadValidationKategori)

      return response.ok({
        message: `Kategori id = ${idKategori} Berhasil Diupdate`
      })
    }
  }


  public async destroy({ response, params }: HttpContextContract) {
    const idKategori = params.id;
    const kategori = await Kategori.findOrFail(idKategori)
    await kategori.delete()

    return response.ok({
      message: `Kategori id = ${idKategori} Berhasil Dihapus`
    })
  }
}
