import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import UserValidator from 'App/Validators/UserValidator';
import User from 'App/Models/User';

export default class UserController {
    public async index({ response }: HttpContextContract) {
        const user = await User.query().orderBy('id', 'asc')

        return response.ok({
            message: 'User Berhasil Ditampilkan',
            data: user
        })

    }

    public async show({ response, params }: HttpContextContract) {
        const idUser = params.id;
        const indexUser = await User.findBy('id', idUser)
        const user = await User.query().preload('buku').where('id', idUser)

        if (!indexUser) {
            return response.notFound({
                message: 'Data Tidak Ditemukan'
            })
        } else {
            return response.ok({
                message: 'Detail User Berhasil Ditampilkan',
                data: user
            })
        }
    }

    public async update({ request, response, params }: HttpContextContract) {
        const idUser = params.id;
        const indexUser = await User.findBy('id', idUser)
        if (!indexUser) {
            return response.notFound({
                message: 'User Tidak Ditemukan'
            })
        } else {
            const payloadValidationUser = await request.validate(UserValidator);
            await User
                .query()
                .where('id', idUser)
                .update(payloadValidationUser)

            return response.ok({
                message: `User id = ${idUser} Berhasil Diupdate`
            })
        }
    }

    public async destroy({ response, params }: HttpContextContract) {
        const idUser = params.id;
        const user = await User.findOrFail(idUser)
        await user.delete()

        return response.ok({
            message: `User id = ${idUser} Berhasil Dihapus`
        })
    }
}

