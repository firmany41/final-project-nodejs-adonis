import { schema, CustomMessages, rules } from '@ioc:Adonis/Core/Validator'
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class BukuValidator {
  constructor(protected ctx: HttpContextContract) { }

  /*
   * Define schema to validate the "shape", "type", "formatting" and "integrity" of data.
   *
   * For example:
   * 1. The username must be of data type string. But then also, it should
   *    not contain special characters or numbers.
   *    ```
   *     schema.string({}, [ rules.alpha() ])
   *    ```
   *
   * 2. The email must be of data type string, formatted as a valid
   *    email. But also, not used by any other user.
   *    ```
   *     schema.string({}, [
   *       rules.email(),
   *       rules.unique({ table: 'users', column: 'email' }),
   *     ])
   *    ```
   */
  public schema = schema.create({
    judul: schema.string([
      rules.alphaNum({
        allow: ['space', 'underscore']
      })
    ]),
    ringkasan: schema.string([
      rules.minLength(10)
    ]),
    tahun_terbit: schema.number([
      rules.range(1, 2023)
    ]),
    halaman: schema.number([
      rules.range(1, 99999999)
    ]),
    kategori_id: schema.number([
      rules.exists({ table: 'kategori', column: 'id' })
    ])
  })

  /**
   * Custom messages for validation failures. You can make use of dot notation `(.)`
   * for targeting nested fields and array expressions `(*)` for targeting all
   * children of an array. For example:
   *
   * {
   *   'profile.username.required': 'Username is required',
   *   'scores.*.number': 'Define scores as valid numbers'
   * }
   *
   */
  public messages: CustomMessages = {
    required: 'inputan {{ field }} tidak boleh kosong',
    'judul.alphaNum': 'inputan hanya boleh berupa alphabet',
    'ringkasan.minLength': 'panjang karakter tidak boleh kurang dari 10',
    'tahun_terbit.range': 'tahun terbit harus antara sebelum tahun 2023 hingga tahun 2023',
    'halaman.range': 'minimal harus 1 halaman',
    'kategori_id.exists': 'id kategori tidak ditemukan'
  }
}
