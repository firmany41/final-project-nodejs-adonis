import { DateTime } from 'luxon'
import { BaseModel, BelongsTo, ManyToMany, belongsTo, column, manyToMany } from '@ioc:Adonis/Lucid/Orm'
import Kategori from './Kategori'
import User from './User'

export default class Buku extends BaseModel {
  public static table = 'buku'

  @column({ isPrimary: true })
  public id: number

  @column()
  public judul: string

  @column()
  public ringkasan: string

  @column()
  public tahun_terbit: number

  @column()
  public halaman: number

  @column()
  public kategori_id: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(() => Kategori,{
    foreignKey: "kategori_id"
  }) 
  public kategori: BelongsTo<typeof Kategori>

  @manyToMany(() => User,{
    pivotTable: 'peminjaman',
    localKey: 'id',
    pivotForeignKey: 'buku_id',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'user_id',
  }) 
  public peminjaman: ManyToMany<typeof User>
}
